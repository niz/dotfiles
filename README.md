# dotfiles

[![License MIT](https://img.shields.io/badge/License-MIT-success.svg)](https://codeberg.org/niz/dotfiles/src/branch/main/LICENSE)

> This is my dotfiles repo. There are many like it, but this one is mine.

## Description

I'm not a big configurations guy, since I usually don't have to access that many systems and I do okay with my regular settings. However, a man has got to have some dotfiles!


These are my personal configs and scripts, mostly for my own use. Feel free to copy whatever is useful to you.
