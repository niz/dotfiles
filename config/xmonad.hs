-- --------------------------------------------------------------
--   My XMonad Stuff
-- --------------------------------------------------------------
-- Author: Niz
-- Repository: https://codeberg.org/niz/dotfiles
--
-- These configs are heavily based on the official tutorial.
--   <https://xmonad.org/TUTORIAL.html>
--
-- Some ideas also inspired by DT's configs.
--   <https://gitlab.com/dwt1/dotfiles>

import XMonad

import XMonad.Util.Ungrab

import XMonad.Util.EZConfig
import XMonad.Util.Loggers
import XMonad.Util.Run


import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.StatusBar
import XMonad.Hooks.StatusBar.PP

import XMonad.Layout.Reflect


-- The preferred terminal program.
myTerminal = "xterm"

-- Whether focus follows the mouse pointer.
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = False

-- Whether clicking on a window to focus also passes the click to the window
myClickJustFocuses :: Bool
myClickJustFocuses = False

-- Width of the window border in pixels.
myBorderWidth   = 2

-- Border colors for unfocused and focused windows, respectively.
myNormalBorderColor  = "#928374"
myFocusedBorderColor = "#b8bb26"

-- The default number of workspaces (virtual screens) and their names.
myWorkspaces    = ["1","2","3","4","5","6","7","8","9"]

------------------------------------------------------------------------

-- Layouts:
myLayout = avoidStruts ( tiled ||| Mirror tiled ||| flipTiled ||| Full)
  where
     -- default tiling algorithm partitions the screen into two panes
     tiled   = Tall nmaster delta ratio

     -- The default number of windows in the master pane
     nmaster = 1

     -- Percent of screen to increment by when resizing panes
     delta   = 3/100

     -- Default proportion of screen occupied by master pane
     ratio   = 1/2

     -- Flips Tall layout
     flipTiled = reflectHoriz $ tiled


------------------------------------------------------------------------

myStartupHook :: X ()
myStartupHook = do
     -- Run startup script for each machine.
     spawn "$HOME/xmonad.start"


myPP = def { ppCurrent = xmobarColor "yellow" "" . wrap "[" "]"
           , ppTitle   = xmobarColor "#fe8019" "" . shorten 40
           , ppVisible = wrap "(" ")"
           , ppUrgent  = xmobarColor "red" "yellow"
           }

mySB = statusBarProp "xmobar" (pure myPP)


main :: IO ()
main = do
  -- Launching two instances of xmobar on their monitors.
  xmproc0 <- spawnPipe ("xmobar -x 0 $HOME/.config/xmobar/xmobarrc")
  xmproc1 <- spawnPipe ("xmobar -x 1 $HOME/.config/xmobar/xmobarrc")
  xmonad . withSB mySB . ewmh . docks $ defaults

defaults = def
    {
        terminal           = myTerminal,
        startupHook        = myStartupHook,
        focusFollowsMouse  = myFocusFollowsMouse,
        clickJustFocuses   = myClickJustFocuses,
        borderWidth        = myBorderWidth,
        normalBorderColor  = myNormalBorderColor,
        focusedBorderColor = myFocusedBorderColor,
        workspaces         = myWorkspaces,
        layoutHook         = myLayout
    }
  `additionalKeysP`
    [   ("M-p", spawn "dmenu_run -nb '#282828' -sf '#282828' -sb '#b8bb26' -nf '#EBDBB2'")
      , ("M-o", unGrab *> spawn "scrot -s ~/Pictures/Screenshots/%F_%H-%M-%S.png")
    ]
