" --------------------------------------------------------------
"   My Vim Stuff
" --------------------------------------------------------------
" Author: Niz
" Repository: https://codeberg.org/niz/dotfiles
"
"   I've taken a lot examples and suggestions from posts and
"   tutorials I've found online while researching the subject.
"
"   Where to place this file:
"     Vim: ~/.vimrc
"     Neovim: ~/.config/nvim/init.vim
"
"   To reload Vim configurations without a restart run:
"     :source $MYVIMRC
"
"   Using vim-tiny detection with ``has("eval")`` from rwxrob:
"     https://github.com/rwxrob/dot
" --------------------------------------------------------------

" BASICS
" --------------------------------------------------------------
" Disable Vi compatibility.
set nocompatible

" Backspace behavior different to Vi.
set backspace=indent,eol,start

" Redraw tweak for smoother and fast macros.
set lazyredraw

" Set the number commands to save in history, default is 20.
set history=100

" Search down into sub folders.
" Provides tab completion for all file related tasks.
set path+=**

" Disable bell sounds.
set belloff=all


" INTERFACE
" --------------------------------------------------------------
" Display file name in window title.
set title

" Displays line numbers.
set number

" Do not let cursor scroll below or above N number of lines
" when scrolling.
set scrolloff=10

" Show wild menu.
set wildmenu

" Make wildmenu behave similar to Bash completion.
set wildmode=list:longest

" Wildmenu will ignore files with these extensions.
set wildignore=*.docx,*.jpg,*.png,*.gif,*.pdf,*.pyc,*.exe,*.flv,*.img,*.xlsx

" While searching through a file incrementally highlight matching
" characters as you type.
set incsearch

" Ignore capital letters during search.
set ignorecase

" Override the ignorecase option if searching for capital letters.
" This will allow you to search specifically for capital letters.
set smartcase

" Show partial command you type in the last line of the screen.
set showcmd

" Show the mode you are on the last line.
set showmode

" Show matching words during a search.
set showmatch

" Use highlighting when doing a search.
set hlsearch

" Enable type file detection.
filetype on

" Enable plugins and load plugin for the detected file type.
filetype plugin on

" Load an indent file for the detected file type.
filetype indent on


" TEXT AND FORMATTING
" --------------------------------------------------------------
" Use Unicode.
set encoding=utf-8

" Set shift width to 4 spaces.
set shiftwidth=4

" Set tab width to 4 columns.
set tabstop=4

" Use space characters instead of tabs.
set expandtab

" Do not wrap lines
set nowrap

" Auto indenting on new line for c like programs
set autoindent smartindent

" If it isn't vim-tiny.
if has("eval")

  " Enable spell checking.
  set spell

  " Set default languages as both American English and Brazilian Portuguese.
  set spell spelllang=en_us,pt_br

  " Remove trailing white spaces on save.
  "   <https://stackoverflow.com/a/1618401>
  function! <SID>StripTrailingWhitespaces()
    if !&binary && &filetype != 'diff'
      let l:save = winsaveview()
      keeppatterns %s/\s\+$//e
      call winrestview(l:save)
    endif
  endfunction

  autocmd BufWritePre,FileWritePre,FileAppendPre,FilterWritePre *
    \ :call <SID>StripTrailingWhitespaces()

endif


" PLUGIN MANAGER
" --------------------------------------------------------------
" If it isn't vim-tiny.
if has("eval")

  " Plugin Manager: vim-plug
  " Automatically install if missing.
  "   <https://github.com/junegunn/vim-plug/wiki/tips#automatic-installation>
  let data_dir = has('nvim') ? '~/.local/share/nvim/site/' : '~/.vim'
  if empty(glob(data_dir . '/autoload/plug.vim'))
    silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  endif

  " Echo a message if there are missing plugins.
  autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
    \| echo 'Some plugins are missing. Run PlugInstall to install.'
  \|

  call plug#begin(has('nvim') ? '~/.local/share/nvim/plugged' : '~/.vim/plugged')

  Plug 'ap/vim-css-color'
  Plug 'morhetz/gruvbox'
  Plug 'mzlogin/vim-markdown-toc'
  Plug 'scrooloose/nerdtree'
  Plug 'sheerun/vim-polyglot'
  Plug 'vim-airline/vim-airline'
  Plug 'junegunn/goyo.vim'

  call plug#end()

endif


" PLUGIN SETUP
" --------------------------------------------------------------
" NERDTree configurations.
let NERDTreeShowHidden = 1
let NERDTreeMinimalUI = 1
let NERDTreeDirArrows = 1

"NERDTree shortcut remapings.
nnoremap <leader>n :NERDTreeFocus<CR>
nnoremap <C-t> :NERDTreeToggle<CR>
nnoremap <C-f> :NERDTreeFind<CR>


" Set up line wrap when Goyo is active.
function! s:goyo_enter()
  set wrap linebreak
endfunction

autocmd! User GoyoEnter nested call <SID>goyo_enter()


" THEME AND COLORS
" --------------------------------------------------------------
" If it isn't vim-tiny.
if has("eval")

  " Enable true colors
  if (has("nvim"))
    " For Neovim 0.1.3 and 0.1.4 <https://github.com/neovim/neovim/pull/2198>
    let $NVIM_TUI_ENABLE_TRUE_COLOR=1
  endif
  " For Neovim > 0.1.5 and Vim > patch 7.4.1799 <https://github.com/vim/vim/commit/61be73bb0f965a895bfb064ea3e55476ac175162>
  " Based on Vim patch 7.4.1770 (`guicolors` option) <https://github.com/vim/vim/commit/8a633e3427b47286869aa4b96f2bfc1fe65b25cd>
  "   <https://github.com/neovim/neovim/wiki/Following-HEAD#20160511>
  if (has("termguicolors"))
    set termguicolors
  endif

  " Set colorscheme.
  try
    execute 'colorscheme gruvbox'
  " Catch exception if colorscheme isn't available.
  "   <https://vi.stackexchange.com/questions/18932/detecting-whether-colorscheme-is-available>
  catch /^Vim\%((\a\+)\)\=:E185/
    colorscheme default
    set background=dark
  endtry

  " Enable syntax highlighting. Improved by vim-polyglot.
  syntax on

endif
